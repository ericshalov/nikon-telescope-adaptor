nikon-telescope-adaptor
=======================
Nikon Telescope Adaptor - a 3D printable adaptor to attach a Nikon S9300 to
a standard 1.25" telescope eyepiece holder.

![Adaptor diagram](images/nikon_telescope_adaptor1.png "Adaptor diagram")

Downloads:
----------
Clone the repo:

$ git clone 'https://github.com/EricShalov/nikon-telescope-adaptor.git'

Files:
------
nikon_telescope_adaptor1.scad - OpenSCAD source
nikon_telescope_adaptor1.stl - rendered STL object
nikon_telescope_adaptor1.gcode - gcode file for printrbot simple

License:
--------
nikon-telescope-adaptor is licensed under the <a href="http://opensource.org/licenses/BSD-3-Clause">BSD 3-Clause License</a>.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name nikon-telescope-adaptor, nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.
