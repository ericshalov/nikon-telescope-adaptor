// nikon_telescope_adaptor1.scad
//
// Adaptor for Nikon COOLPIX S9300 16MPixel
// camera to 1.5" telescope eyepiece port
// Tested on 8" Sky-Watcher Newtonian.
//
// Code for OpenSCAD. (All measurements in mm)
// Copyright (c) 2015, Eric Shalov. All rights reserved.

union() {
	// tube that fits into 
	difference() {
		cylinder(h=20, r=15.75, center=true, $fn=100);
		cylinder(h=50, r=14.75, center=true, $fn=8);
	}
	difference() {
		translate([0,0,-10]) {	
            cylinder(h=3, r=23.0, center=true, $fn=24);
		}
		cylinder(h=50, r=14, center=true, $fn=8);
	}
}

translate([0,0,-25+1.0]) {	
    difference() {
		cylinder(h=25, r=23, center=true, $fn=24);
		cylinder(h=50, r=21, center=true, $fn=100);
    }
}
